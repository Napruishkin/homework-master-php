<?php

header('Content-Type: application/json');

$requestMethod = $_SERVER['REQUEST_METHOD'];
$requestBody = file_get_contents('php://input');
$requestData = json_decode($requestBody, true);

if ($requestMethod!== 'GET' && $requestMethod!== 'POST') {
    http_response_code(405);
    echo json_encode(['error' => 'Method not allowed']);
    exit;
}

if ($requestMethod === 'POST') {
    if (json_last_error()!== JSON_ERROR_NONE) {
        http_response_code(422);
        echo json_encode(['error' => 'Request must contain valid JSON data.']);
        exit;
    }

    $keywords = $requestData['keywords']?? [];

    // Replace this function with your actual implementation to fetch articles
    function get_articles_with_keywords($keywords)
    {
        // Fetch articles from the database or external API
        //...

        // For demonstration purposes, return some dummy data
        return [
            (object)['header' => 'Article 1', 'url' => 'https://example.com/article1'],
            (object)['header' => 'Article 2', 'url' => 'https://example.com/article2'],
            (object)['header' => 'Article 3', 'url' => 'https://example.com/article3'],
        ];
    }

    $articles = get_articles_with_keywords($keywords);

    $responseData = [
        'articles' => array_map(
            function ($article) {
                return [
                    'text' => $article->header,
                    'url' => $article->url,
                ];
            },
            $articles
        ),
    ];

    http_response_code(200);
    echo json_encode($responseData);
}