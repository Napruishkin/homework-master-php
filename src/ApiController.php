<?php

namespace App;

use App\ArticleService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends AbstractController
{
    private $articleService;

    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService;
    }

    /**
     * @Route("/find-articles", methods={"POST"})
     */
    public function findArticles(Request $request): JsonResponse
    {
        // Check if the request contains JSON data
        if (!$request->isJson()) {
            return new JsonResponse(['error' => 'Request must contain JSON data.'], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Extract keywords from the JSON payload
        $keywords = $request->get('keywords', []);

        return new JsonResponse([
            'articles' => $this->articleService->getArticlesWithKeywords($keywords)
        ]);
    }
}