<?php

namespace App;

use App\Scraper\ResonancedailyScraper;
use App\Scraper\GeorgiatodayScraper;
use App\Scraper\BbcScraper;
use App\Scraper\IdnesScraper;
use Psr\Log\LoggerInterface;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class WebScrapingService
{
    private $log;

    public function __construct(LoggerInterface $logger)
    {
        $this->log = $logger;
    }

    public function scrapeNews()
    {
        $scrapers = [
            new ResonancedailyScraper(),
            new GeorgiatodayScraper(),
            new BbcScraper(),
            new IdnesScraper()
        ];

        foreach ($scrapers as $scraper) {
            $this->log->info("Scraping news using " . get_class($scraper));

            try {
                $articles = $scraper->getHeaders();
                foreach ($articles as $article) {
                    $this->saveArticleIfNew($article);
                }
            } catch (\Exception $e) {
                $this->log->error("Error while scraping " . get_class($scraper) . ": " . $e->getMessage());
            }
        }
    }

    private function saveArticleIfNew($article)
    {
        // Implementation for saving article if it's new
    }
}

$logger = new \Monolog\Logger('web_scraping');
$logger->pushHandler(new \Monolog\Handler\StreamHandler(__DIR__.'/../logs/web_scraping.log', \Monolog\Logger::INFO));

$webScrapingService = new WebScrapingService($logger);
$webScrapingService->scrapeNews();
