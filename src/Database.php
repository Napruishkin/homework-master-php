<?php

use App\Entity\Article;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\DBAL\Connection;

function create_empty_db(EntityManager $entityManager) {
    $schema = $conn->getSchemaManager()->createSchema();

    // Create a SchemaTool object
    $schemaTool = new SchemaTool($conn);

    // Get an array of all table names in the database
    $tables = $schema->getTables();

    // Loop through the array of table names and drop each table
    foreach ($tables as $table) {
        $schemaTool->dropTable($table);
    }

    // Execute the SQL queries to drop the tables
    $schemaTool->executeSchemaAction(SchemaTool::DROP);

    $schema = $conn->getSchemaManager()->createSchema();

    // Get an array of all entity classes in the App\Entity namespace
    $entityClasses = [Article::class];

    // Get a Schema object representing the schema for the entity classes
    $targetSchema = $schema->createSchema();
    $metadata = $conn->getMetadataFactory()->getAllMetadata();
    foreach ($metadata as $classMetadata) {
        $table = new \Doctrine\DBAL\Schema\Table($classMetadata->table['name']);
        foreach ($classMetadata->columnNames as $columnName) {
            $column = new \Doctrine\DBAL\Schema\Column($columnName);
            $column->setType($classMetadata->columnTypes[$columnName]);
            $table->addColumn($column);
        }
        $targetSchema->addTable($table);
    }

    // Compare the current database schema with the target schema and generate SQL queries
    $sqls = $schemaTool->getCreateSchemaSql($targetSchema, $schema);

    // Execute the SQL queries to create the tables
    foreach ($sqls as $sql) {
        $conn->exec($sql);
    }
}
