<?php

namespace App\Model;


/**
 * Represents an article from a news server.
 */
class Article
{
    public string $header;
    public string $url;

    public function __construct(string $header, string $url)
    {
        $this->header = $header;
        $this->url = $url;
    }
}
