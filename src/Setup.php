<?php

use Doctrine\DBAL\DriverManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMSetup;

require_once "vendor/autoload.php";
require_once __DIR__ . '/Database.php';

if (PHP_SAPI === 'cli') {
    $config = ORMSetup::createAttributeMetadataConfiguration(
        paths: array(__DIR__),
        isDevMode: true,
    );

    $connection = DriverManager::getConnection([
        'driver' => 'pdo_pgsql',
        'host' => 'localhost',
        'port' => '5432',
        'user' => 'postgres',
        'password' => 'postgres',
        'dbname' => 'postgres',
    ], $config);
    $entityManager = new EntityManager($connection, $config);

    create_empty_db($entityManager);
    echo 'Created an empty DB';
} else {
    echo 'This script can only be executed from the command line.';
}
