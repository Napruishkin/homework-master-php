<?php

namespace App;

use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;

class ArticleService
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    public function getArticlesWithKeywords(array $keywords): array
    {
        if (empty($keywords)) {
            return [];
        }

        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder->select('a')
            ->from(Article::class, 'a');

        $expr = $queryBuilder->expr();
        $orConditions = [];
        foreach ($keywords as $keyword) {
            $orConditions[] = $expr->like('a.header', $expr->literal('%' . $keyword . '%'));
        }

        $queryBuilder->andWhere($expr->orX(...$orConditions))
            ->orderBy('a.timestamp', 'DESC');

        return $queryBuilder->getQuery()->getResult();
    }

    public function saveArticleIfNew(Article $article): void
        {
            $existingArticle = $this->entityManager->getRepository(Article::class)->findOneBy(['url' => $article->getUrl()]);

            if ($existingArticle === null) {
                $this->entityManager->persist($article);
                $this->entityManager->flush();
            }
    }
}