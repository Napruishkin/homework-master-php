<?php

namespace App\Scraper;

use App\Model\Article;
use Symfony\Component\DomCrawler\Crawler;

class ResonancedailyScraper extends NewsScraper
{
    /**
     * @return Article[]
     */
    public function getHeaders(): array
    {
        $url = 'https://www.resonancedaily.com/';
        $crawler = new Crawler(file_get_contents($url));

        $articles = [];
        foreach ($crawler->filter('.rb') as $article) {
            $header = $article->filter('a')->text();
            $url = 'https://www.resonancedaily.com/'. $article->filter('a')->attr('href');
            $articles[] = new Article($header, $url);
        }

        return $articles;
    }
}
