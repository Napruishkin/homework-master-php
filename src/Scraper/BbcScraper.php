<?php

namespace App\Scraper;

use App\Model\Article;
use Symfony\Component\DomCrawler\Crawler;

class BbcScraper extends NewsScraper
{
    /**
     * @return Article[]
     */
    public function getHeaders(): array
    {
        $url = 'https://www.bbc.com/news';
        $crawler = new Crawler(file_get_contents($url));

        $articles = [];
        foreach ($crawler->filter('[data-testid="edinburgh-card"]') as $article) {
            if (!$article->filter('a')->attr('href')) {
                continue;
            }
            $header = $article->filter('[data-testid="card-headline"]')->text();
            $url = 'https://www.bbc.com'. $article->filter('a')->attr('href');
            $articles[] = new Article($header, $url);
        }

        return $articles;
    }
}