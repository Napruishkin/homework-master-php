<?php

namespace App\Scraper;

use App\Model\Article;
use Symfony\Component\DomCrawler\Crawler;

class GeorgiatodayScraper extends NewsScraper
{
    /**
     * @return Article[]
     */
    public function getHeaders(): array
    {
        $url = 'https://georgiatoday.ge/category/news/';
        $crawler = new Crawler(file_get_contents($url));

        $articles = [];
        foreach ($crawler->filter('.jeg_post') as $article) {
            $header = $article->filter('.jeg_post_title')->text();
            $url = $article->filter('a')->attr('href');
            $articles[] = new Article($header, $url);
        }

        return $articles;
    }
}