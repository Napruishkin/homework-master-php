<?php

namespace App\Scraper;

use App\Model\Article;
use Symfony\Component\DomCrawler\Crawler;

class IdnesScraper extends NewsScraper
{
    /**
     * @return Article[]
     */
    public function getHeaders(): array
    {
        $url = 'https://www.idnes.cz/';
        $crawler = new Crawler(file_get_contents($url));

        $articles = [];
        foreach ($crawler->filter('.art') as $article) {
            if (!$article->filter('h3')) {
                continue;
            }
            $header = $article->filter('h3')->text();
            $url = $article->filter('a')->attr('href');
            $articles[] = new Article($header, $url);
        }

        return $articles;
    }
}