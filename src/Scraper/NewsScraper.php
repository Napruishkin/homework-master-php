<?php

namespace App\Scraper;

/**
 * Abstract class for news scrapers.
 */
abstract class NewsScraper
{
    abstract public function getHeaders(): array;
}
